package com.tjddm.eduardo.bilk.exerciciospinner;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


public class ExSpinner extends Activity implements OnItemSelectedListener {
	private Spinner spinnerDir;
	private Spinner spinnerArq;
	private String[] diretorios;
	private String[] arquivos;
	private File dir;
	private ArqListener arqListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex_spinner);
        spinnerDir = (Spinner) findViewById(R.id.spinner1);
        spinnerArq = (Spinner) findViewById(R.id.spinner2);
        diretorios = Environment.getExternalStorageDirectory().list();
        ArrayAdapter<String> adpDir = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1,diretorios);
        spinnerDir.setAdapter(adpDir);
        spinnerDir.setOnItemSelectedListener(this);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ex_spinner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
		String diretorioEscolhido = (String) parent.getItemAtPosition(pos);
		dir = new File(Environment.getExternalStorageDirectory()+"/"+diretorioEscolhido);
		arquivos = dir.list();
		ArrayAdapter<String> adpArq = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, arquivos);
		spinnerArq.setAdapter(adpArq);
		arqListener = new ArqListener(this, dir);
		spinnerArq.setOnItemSelectedListener(arqListener);
	}


	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
}
class ArqListener implements OnItemSelectedListener{
	private Context contexto;
	private File diretorio;
	private File arquivo;
	
	private String mensagem;
	
	public ArqListener(Context ctx, File dir){
		contexto = ctx;
		diretorio = dir;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos,
			long id) {
		String arquivoSelecionado = (String) parent.getItemAtPosition(pos);
		arquivo = new File(diretorio.getAbsolutePath()+"/"+arquivoSelecionado);
		
		mensagem = 	"Diretório: "+arquivo.getParent().toString()+"\n"+
					"Nome do Arquivo: "+arquivo.getName().toString()+"\n"+
					"Tamanho: "+arquivo.length()+" bytes"+"\n"+
					"Executar: "+arquivo.canExecute()+"\n"+
					"Ler: "+arquivo.canRead()+"\n"+
					"Escrever: "+arquivo.canWrite()+"\n";
		
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(contexto);		
		dialogBuilder.setMessage(mensagem)
					.setTitle("Infos do arquivo Selecionado")
					.setIcon(android.R.drawable.ic_dialog_info)
					.show();		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	
	
}
